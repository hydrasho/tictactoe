public class GameChecker {
	public GameChecker(Grid new_grid) {
		this.grid = new_grid;
	}

	bool checkRow(int i, TileType type = NONE) {
		bool	res = (grid[i * 3] == grid[i * 3 + 1]
				&& grid[i * 3] == grid[i * 3 + 2]);
		if (type == NONE)
			return res && grid[i * 3] != NONE;
		else
			return res && grid[i * 3] == type;
	}

	bool checkCol(int i, TileType type = NONE) {
		bool	res = (grid[i] == grid[i + 3]
				&& grid[i] == grid[i + 6]);
		if (type == NONE)
			return res && grid[i] != NONE;
		else
			return res && grid[i] == type;
	}

	bool checkDiag(int i, TileType type = NONE) {
		bool	res = (grid[4] == grid[4 -  i]
				&& grid[4] == grid[4 + i]);
		if (type == NONE)
			return res && grid[4] != NONE;
		else
			return res && grid[4] == type;
	}

	public bool hasWinner(TileType t = NONE) {
		for (int i = 0; i < 3; i++) {
			if (checkRow(i, t) || checkCol(i, t))
				return true;
		}
		if (checkDiag(2, t) || checkDiag(4, t))
			return true;
		return false;
	}

	public bool gameFinished(Grid? new_grid = null) {
		if (new_grid != null)
			this.setGrid(new_grid);
		if (hasWinner()) {
			if (hasWinner(CIRCLE))
				winner = CIRCLE;
			else if (hasWinner(CROSS))
				winner = CROSS;
			return true;
		}
		foreach (var elem in grid.data)
			if (elem == NONE)
				return false;
		winner = NONE;
		return true;
	}

	public void setGrid(Grid new_grid) {
		this.grid = new_grid;
	}

	Grid	grid; 
	public TileType		winner {get; private set;}
}

