public class ExampleApp : Gtk.Application {
	public ExampleApp () { }
	construct {
		application_id = "com.morpion.app";
	}

	public override void activate () {
		var provider = new Gtk.CssProvider();
		provider.load_from_resource("/data/style.css");
		Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

		var win = new Gtk.ApplicationWindow (this) {
			default_height = 440,
			default_width = 400,
		};
		
		win.set_child (new Game(win));

		win.present ();
	}

	public static int main (string[] args) {
		var app = new ExampleApp ();
		return app.run (args);
	}
	}
