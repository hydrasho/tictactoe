public class Grid {
	public TileType	[]data;

	public Grid(TileType []new_data) {
		this.data = new_data;
	}

	public string to_string() {
		var bs = new StringBuilder.sized (16);

		for (int i = 0; i < 9; ++i) {
			bs.append(@"$(data[i])");

			if (i % 3 == 2)
				bs.append("\n");
			else
				bs.append(" ");
		}
		return (owned)bs.str;
	}

	public Grid copy() {
		return new Grid(this.data.copy());
	}

	public TileType	get (int index) {
		return this.data[index];
	}

	public void	set (int index, TileType t) {
		this.data[index] = t;
	}
}
