using Gtk;

public class Game : Box {
	construct {
		orientation = VERTICAL;
	}

	unowned Window	window;
	
	/* Init the game */
	public Game (Window p) {
		window = p;
		this.grid = new WidgetGrid();
		this.checker = new GameChecker(grid.get_array());
		new_game();

		var title2P = new Label("2 Players");
		var	button_new_game = new Gtk.Button.with_label ("Play!");
		var titleBot = new Label("Against Bot");
		var box_ia = new Box(Orientation.HORIZONTAL, 10);
		var	b_game_ia_easy = new Gtk.Button.with_label ("   Easy   ");
		var	b_game_ia_hard = new Gtk.Button.with_label ("  Medium  ");
		var	b_game_ia_imp = new Gtk.Button.with_label  ("Impossible");
		
		grid.onClick.connect ((id) => {
			onClick(id);
			checkGrid();
			if (bot != null) {
				int iaPos = bot.playTurn(grid.get_array());
				grid[iaPos] = CIRCLE; 
				checkGrid();
				is_cross_turn = !is_cross_turn;
			}
		});

		button_new_game.clicked.connect(()=> {
			new_game ();
		});

		b_game_ia_easy.clicked.connect(() => {
			new_game(true, 1);
		});

		b_game_ia_hard.clicked.connect(() => {
			new_game(true, 3);
		});

		b_game_ia_imp.clicked.connect(() => {
			new_game(true);
		});

		box_ia.halign = CENTER;
		box_ia.baseline_position = CENTER;

		base.append(title2P);
		base.append(button_new_game);
		base.append(titleBot);
		base.append(box_ia);
		box_ia.append(b_game_ia_easy);
		box_ia.append(b_game_ia_hard);
		box_ia.append(b_game_ia_imp);
		base.append(grid);
	}

	void checkGrid() {
		if (checker.gameFinished(this.grid.get_array()))
			displayWinner(checker.winner);
	}


	/* When a new game is started */
	void new_game(bool againstBot = false, int difficulty = -1) {
		if (!againstBot)
			window.title = "Tic Tac Toe - 2P";
		else if (difficulty == 1)
			window.title = "Tic Tac Toe - Easy";
		else if (difficulty == 3)
			window.title = "Tic Tac Toe - Medium";
		else if (difficulty == -1)
			window.title = "Tic Tac Toe - Impossible";

		grid.erase();
		bot = null;
		is_cross_turn = true;
		if (againstBot)
			bot = new Bot(CIRCLE, difficulty);
		checker.setGrid(this.grid.get_array());
	}
	
	void displayWinner(TileType type) {
		for (int i = 0; i < 9; i++) {
			grid[i] = type;
		}
	}

	/* When a tile is clicked id is the number of tiles*/
	void onClick (int id) {
		if (grid[id] != NONE)
			return ;
		if (is_cross_turn)
			grid[id] = CROSS; 
		else
			grid[id] = CIRCLE; 
		is_cross_turn = !is_cross_turn;
	}


	public	bool 		is_cross_turn = true;
	private WidgetGrid	grid;
	private GameChecker checker;
	private Bot?		bot;
}

