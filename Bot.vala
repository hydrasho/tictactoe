const int scores[3] = {0, 10, -10};

class Bot {
	public Bot(TileType type, int depth = -1) {
		_type = type;
		if (_type == CIRCLE)
			_opp_type = CROSS;
		else
			_opp_type = CIRCLE;
		_max_depth = depth - 1;
	}

	private int	minimax(Grid grid, int depth, bool isMaximizing) {
		GameChecker gc = new GameChecker(grid);
		int			bestScore;

		if (gc.gameFinished())
			return scores[gc.winner];
		else if (depth == _max_depth)
			return 0;
		if (isMaximizing) {
			bestScore = -42;
			for (int i = 0; i < 9; i++) {
				if (grid[i] != NONE)
					continue;
				grid[i] = this._type;
				var score = minimax(grid, depth + 1, false);
				grid[i] = NONE;

				bestScore = int.max(score, bestScore);
			}
		} else {
			bestScore = 42;
			for (int i = 0; i < 9; i++) {
				if (grid[i] != NONE)
					continue;
				grid[i] = this._opp_type;
				var score = minimax(grid, depth + 1, true);
				grid[i] = NONE;

				bestScore = int.min(score, bestScore);
			}
		}
		return bestScore;
	}

	public int playTurn(Grid grid) {
		int bestScore = -42;
		int	bestMove = 0;

		for (int i = 0; i < 9; i++) {
			if (grid[i] != NONE)
				continue ;
			var new_data = grid.copy();
			new_data[i] = _type;
			int score = minimax(new_data, 0, false);

			if (score > bestScore) {
				bestScore = score;
				bestMove = i;
			}
		}
		// print("\n%s\nScore:%d\nMove: %d\n", grid.to_string(), bestScore, bestMove);
		return bestMove;
	}

	private TileType	_type = NONE;
	private TileType	_opp_type = NONE;
	private int			_max_depth;
}
