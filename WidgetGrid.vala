public enum TileType {
	CROSS = 2,
	CIRCLE = 1,
	NONE = 0;

	public unowned string to_string() {
		switch (this) {
			case CROSS:
				return "x";
			case CIRCLE:
				return "o";
			default:
			case NONE:
				return " ";
		}
	}
}


public class WidgetGrid : Gtk.Grid {
	construct {
		row_homogeneous = true;
		column_homogeneous = true;
		hexpand = true;
		vexpand = true;
	}

	public WidgetGrid () {
		// Init all Tiles
		buttons = {};
		for (int i = 0; i < 9; i++)
			buttons += new Tiles(i);
		
		init_event();
		erase ();
		// Init event Tiles
	}

	public Iterator iterator() {
        return new Iterator(this);
    }

	public class Iterator {
        private int index;
		private WidgetGrid grid; 

        public Iterator(WidgetGrid grid) {
			this.grid = grid;
			this.index = 0;
        }

        public bool next() {
			if (index >= 9)
				return false;
            return true;
        }

        public TileType get() {
            this.index++;
            return this.grid[this.index - 1];
        }
    }	

	public string to_string () {
		var bs = new StringBuilder.sized (32);
		int n = 0;
		foreach (var i in buttons) {
			if (i.get_tiletype () == NONE)
				bs.append("_ ");
			else if (i.get_tiletype () == CROSS)
				bs.append("X ");
			else if (i.get_tiletype () == CIRCLE)
				bs.append("O ");
			if (n % 3 == 2)
				bs.append("\n");
			++n;
		}
		return bs.str;
	}

	public string to_board() {
		var bs = new StringBuilder.sized (32);
		int n = 0;
		foreach (var i in buttons) {
			if (i.get_tiletype () == NONE)
				bs.append(" ");
			else if (i.get_tiletype () == CROSS)
				bs.append("X");
			else if (i.get_tiletype () == CIRCLE)
				bs.append("O");
			++n;
		}
		return bs.str;
	}


	/* Erase all input */
	public void erase () {
		foreach (var i in buttons) {
			i.set_tiletype (NONE);
		}
	}

	void init_event() {
		var n = 0;
		for (int i = 0; i < 9; i++) {
			base.attach (buttons[i], i % 3, n);
			buttons[i].onClick.connect((v)=> {
				onClick(v);
			});
			if (i % 3 == 2)
				n++;
		}
	}

	public Grid get_array() {
		TileType	data[9];
		for (int i = 0; i < 9; i++)
			data[i] = this[i];
		return new Grid(data);
	}


	public new void set (int index, TileType type) {
		buttons[index].set_tiletype (type);
	}
	public new TileType get (int index) {
		return buttons[index].get_tiletype ();
	}
	
	public signal void onClick(int value);
	private Tiles []buttons;
}

public class Tiles : Gtk.Button{

	construct {
		has_frame = false;
	}

	public Tiles (int id) {
		this.id = id;
		image = new Gtk.Image();
		// image.set_pixel_size (64);
		this.clicked.connect(()=> {
			onClick(id);
		});
		base.set_child (image);
	}

	public TileType get_tiletype() {
		return type;
	}

	public void set_tiletype(TileType type) {
		this.type = type;
		if (type == CROSS) {
			// image.set_from_icon_name ("terminal");
			image.css_classes = {"cross_type"};
		}
		else if (type == CIRCLE) {
			// image.set_from_icon_name ("google-chrome");
			image.css_classes = {"circle_type"};
		}
		else {
			image.clear();
			image.css_classes = {"none_type"};
		}
	}

	public signal void onClick(int value);

	private Gtk.Image image;
	private TileType type;
	private int id;
	
}
